Disclaimer
KCD is a new game that is going to be patched heavily over the next few months. As such, backup your edits and saves regularly and be prepared for changes. Save files can be found at: C:\Users\USERNAME\Saved Games\kingdomcome


Installation
Download the file
Navigate to your Kingdom Come: Deliverance game folder in the Steam folder. The default is C:\Program Files (x86)\Steam\steamapps\common\KingdomComeDeliverance\
Go to the "Mods" folder, if it doesn't exist create it.
Extract the contents of the zip file into your Mods folder.


Usage
1. Uzip the downloaded file to KingdomComeDeliverance\Mod folder [create it iof not exists].
2. Place zzz_BlackArmor.pak to Data directory of your KC:D installation
3. Place items.txt to Data directory of your KC:D installation
4. Place zzz_BlackArmor.pak to Data/_fastload/ directory of your KC:D installation
5. Place english_xml.pak to Localization directory of your KC:D installation. Overwrite when prompted (back up the original file first).
6. Install console mod  if you haven't already
7. Run the game
8. Open console with ` key (~, tilde, top left key below esc)
9. In console type: exec items.txt
10. Enjoy your new items
11. Endorse this mod and Midnight Armor mod


Removal
If you're not happy with the mod or it doesn't work, delete the folder that you extracted to your Mod folder: 

KingdomComeDeliverance\Mods\ToxicGreenArmor

Re-Activate your backup language xml.pak files (english_xml.pak german_xml.pak)