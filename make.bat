@echo off
setlocal enableextensions enabledelayedexpansion

set "BUILD_ROOT=%~dp0build\"
set "BAK_ROOT=%~dp0bak\"

if exist "%BUILD_ROOT%ToxicGreenArmor.zip" (
	call :backup-existing-package "%BUILD_ROOT%ToxicGreenArmor.zip"
)
echo.################
echo.Clean-up Build-Dir and Setup building pipeline...
echo.################
if not exist "%BUILD_ROOT%" (
	mkdir "%BUILD_ROOT%"
) else (
	del /q "%BUILD_ROOT%*"
	FOR /D %%p IN ("%BUILD_ROOT%*.*") DO rmdir "%%p" /s /q
)
set "PACKAGE_ROOT=%BUILD_ROOT%ToxicGreenArmor\"
mkdir "%PACKAGE_ROOT%"

call "%~dp0bin\write_manifest.bat" "%PACKAGE_ROOT%"
if NOT %ERRORLEVEL% == 0 goto :error-writing-manifest

echo.
echo.################
echo.Creating Mod-Package from Source...
echo.################
echo.
call 7z.exe a -tzip "%PACKAGE_ROOT%Data\ToxicGreenArmor.pak" ".\src\Libs" ".\src\Objects"
if %errorlevel% NEQ 0 goto :error-exit

echo.
echo.################
echo.Preparing Localization packages...
echo.################
echo.
set "LOCALIZATION_DIR=%PACKAGE_ROOT%Localization\"
mkdir "%LOCALIZATION_DIR%"
call 7z.exe a -tzip "%LOCALIZATION_DIR%German_xml.pak" ".\src\Localization\de\*"
call 7z.exe a -tzip "%LOCALIZATION_DIR%English_xml.pak" ".\src\Localization\en\*"

echo.
echo.################
echo.Copy auto-script[s]...
echo.################
echo.
copy ".\src\items.txt" "%PACKAGE_ROOT%"

rem :: pause
echo.
echo.################
echo.Building delivery package...
echo.################
echo.
call 7z.exe a -tzip "%BUILD_ROOT%ToxicGreenArmor.zip" "%PACKAGE_ROOT%"
echo.
echo.################
echo.Done...
pause

endlocal
exit /b 0


:backup-existing-package <file_to_backup>
echo.################
echo.Backup existing Package...
if not exist "%~1" (
	echo.[ERROR] Unable to find file: [%~1]
	pause
	exit /b 3
)
set "LAST_MOD_DATE=%~t1"
rem :: filename conventions
set "LAST_MOD_DATE=!LAST_MOD_DATE::=-!"
set "LAST_MOD_DATE=!LAST_MOD_DATE:/=-!"
set "LAST_MOD_DATE=!LAST_MOD_DATE:.=-!"
set "newName=ToxicGreenArmor (!LAST_MOD_DATE!).zip"
echo.Backup-Name for last Build: [!newName!]
if not exist "%BAK_ROOT%" mkdir "%BAK_ROOT%"
move ".\build\ToxicGreenArmor.zip" "%BAK_ROOT%!newName!"
echo.################
echo.

exit /b

:error-exit

echo.################
echo.[33mTo run this script "7z packer" is required and set-up path to "7z.exe" in your PATH environment variable.[0m
echo.################
echo.
pause

exit /b

:error-writing-manifest

echo.################
echo.[33mSomething goes wrong on writing manifest file [check cmd output above...].[0m
echo.################
echo.
pause

exit /b
